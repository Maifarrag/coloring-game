var numSquares=6;
var colors=[];
var pickedColor ;
var squares= document.querySelectorAll(".square");
var display= document.getElementById("colorDisplay");
var messageDisplay= document.getElementById("message");
var h1= document.querySelector("h1");
var resetButton= document.getElementById("reset");
var modeButtons = document.querySelectorAll(".mode");
// var easybtn= document.getElementById("easyButton");
// var hardbtn= document.getElementById("hardButton");
//display.textContent= pickedColor;

init();

function init(){
//mode button event listeners
	setupModeButtons();
// setup for the squares
	setupSquares();
//reset the screens
	reset();
}


function setupModeButtons(){
	for(var i =0 ; i< modeButtons.length;i++){
		modeButtons[i].addEventListener("click", function(){
		modeButtons[0].classList.remove("selected");
		modeButtons[1].classList.remove("selected");
		this.classList.add("selected");
		// to update the numsquares for each button
		this.textContent === "Easy" ? numSquares = 3: numSquares = 6;
		reset();
		});
	}
}

function setupSquares(){
	for(var i=0; i<squares.length; i++){
	
		squares[i].addEventListener("click",function(){
		//grab the color of the selected square
			var selected=this.style.backgroundColor;

			//compare the selected with picked
			if(selected === pickedColor){
				messageDisplay.textContent="Correct!";
				changeColors(selected);
				h1.style.backgroundColor= selected;
				//to change the new colors button to play again
				resetButton.textContent="Play Again?";
			}
			else{
				this.style.backgroundColor="#0d0d0d";
				messageDisplay.textContent="Try Again!";
			}
		});
	}
}



function reset(){
	colors= generateRandomColor(numSquares);
	//pick a new random color from array
	pickedColor= pickColor();
	//change the colordisplay to matches the picked color
	display.textContent=pickedColor;
	//change colors of square
	for(var i=0; i<squares.length;i++){
		squares[i].style.backgroundColor=colors[i];
	}
	messageDisplay.textContent="";
	resetButton.textContent="New Colors";
	for(var i=0;i<squares.length;i++){
		if(colors[i]){
			squares[i].style.display="block";
			squares[i].style.backgroundColor=colors[i];
		}
			else{ 
				squares[i].style.display="none";
			}
	} 
	//reset the h1 color
	h1.style.backgroundColor="steelblue";
}


resetButton.addEventListener("click",function(){
	reset();
});


// to change the squares colors to the winning color
function changeColors(color){
	// loop through all squares
	for(var i=0; i<squares.length; i++){
		//change each color to match given one
		squares[i].style.backgroundColor= color;

	}
	
}

// to randomize the winnig color every time
function pickColor(){
 var random= Math.floor(Math.random()*colors.length);
 return colors[random];
}

function generateRandomColor(num){
	//make an array
	var arr = []; 
	//repeat num times
	for (var i=0; i<num;i++){
		//get random colors and push them in arr
		arr.push(randomColor());

	}
	//retrun that array
	return  arr;
}

function randomColor(){
	//pick red from 0 to 255
	var r= Math.floor(Math.random()*256);
	//pick green from 0 to 255
	var g= Math.floor(Math.random()*256);
	// pick blue from 0 to 255
	var b= Math.floor(Math.random()*256);
	// return rgb() format
	return "rgb(" + r + ", " + g + ", " + b + ")"; // take care of spacing issue here because of the comparison we will make to check the correct choice

}

// easybtn.addEventListener("click",function(){
// 	hardbtn.classList.remove("selected");
// 	easybtn.classList.add("selected");
// 	numSquares=3;
// 	colors=generateRandomColor(numSquares);
// 	pickedColor=pickColor();
// 	display.textContent= pickedColor;
// 	for(var i=0; i< squares.length ; i++){
// 		if(colors[i]){
// 			squares[i].style.backgroundColor=colors[i];
// 		}
// 		else{
// 			squares[i].style.display="none";
// 		}
// 	}

// });
// hardbtn.addEventListener("click",function(){
// 	hardbtn.classList.add("selected");
// 	easybtn.classList.remove("selected");
// 	numSquares=6;
// 	colors=generateRandomColor(numSquares);
// 	pickedColor=pickColor();
// 	display.textContent= pickedColor;
// 	for(var i=0; i< squares.length ; i++){
		
// 		squares[i].style.backgroundColor=colors[i];
// 		squares[i].style.display="block"; // 3lshan lma ados hard tany yrg3o 6 squares msh 3
		
// 	}
// });

